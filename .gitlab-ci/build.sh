#!/bin/bash

set -xeuo pipefail

ORDER=(libsearpc libevhtp-seafile ccnet-server seafile-server seahub python-ffmpeg)
ORDER+=(seafile seafile-client)

rm -rf /var/cache/pacman/pkg/
mkdir -p build/pkg/ /var/cache/pacman/
ln -s "$PWD"/build/pkg /var/cache/pacman/pkg
rm -f /usr/share/libalpm/hooks/package-cleanup.hook
pacman -Syu --noconfirm --needed base-devel ccache
echo "build ALL=(ALL) NOPASSWD: /usr/bin/pacman" > /etc/sudoers.d/build
useradd -d "$PWD"/build build
cat <<-EOF > build/.makepkg.conf
	BUILDENV=(ccache)
	MAKEFLAGS="-j$(nproc)"
	PACKAGER="Timothy Reaelli <timothy.redaelli@gmail.com>"
	SRCDEST="$PWD/build/sources"
	PKGEXT=".pkg.tar.zst"
	EOF

cp -a "${ORDER[@]}" build
chown -R build: build

pushd build
for dir in "${ORDER[@]}"; do
	pushd "$dir" >/dev/null
	sudo -u build makepkg -csi --noconfirm
	popd >/dev/null
done
popd
sudo -u build ccache -s
pacman -Sc --noconfirm
exit 0

# FIXME
while read -r dir; do
	pushd "$dir" >/dev/null
	makepkg
	popd >/dev/null
done < <(git diff-tree --name-only HEAD^ HEAD)
